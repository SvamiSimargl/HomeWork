package com.svamisimargl.homework;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity3 extends AppCompatActivity {
    TextView name, lastName, age;
    Button exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity3
        );


        name = findViewById(R.id.textView);
        lastName = findViewById(R.id.lastNameView);
        age = findViewById(R.id.ageView);
        exit = findViewById(R.id.exitBtn);

        Intent intent = getIntent();
        Student student = intent.getParcelableExtra(MainActivity.EXTRA_STUDENT);
        name.setText(student.getName());
        lastName.setText(student.getLastName());
        age.setText(String.valueOf(student.getAge()));

    }
    public void OnClick (View view){
        switch (view.getId()){
            case R.id.exitBtn:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                break;
        }

    }

}
