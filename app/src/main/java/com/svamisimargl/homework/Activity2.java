package com.svamisimargl.homework;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    EditText name, lastName, age;
    Button edit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);

        name = findViewById(R.id.nameEditText);
        lastName = findViewById(R.id.lastNameEditText);
        age = findViewById(R.id.ageEditText);
        edit = findViewById(R.id.editBtn);

//код для получения значений обьекта с активити.
        Intent intent = getIntent();
        Student student = intent.getParcelableExtra(MainActivity.EXTRA_STUDENT);

        //заполняем вьюшки значениями из разпарсенного обьекта
        name.setText(student.getName());
        lastName.setText(student.getLastName());
        age.setText(String.valueOf(student.getAge()));


    }

    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.editBtn:
                //передаем значения нашего обьекта на следующее активити, т.к. вьюшки EditText являются обьектами,
                // а нам нужно заполнить конструктор обьекта (String,String,int), то мы все приводим в соответствующие типы.
                Student student = new Student(String.valueOf(name.getText()), String.valueOf(lastName.getText()), Integer.parseInt(String.valueOf(age.getText())));
                Intent intent = new Intent(this, Activity3.class);
                intent.putExtra(MainActivity.EXTRA_STUDENT, student);
                startActivity(intent);
                break;
        }
    }

}
