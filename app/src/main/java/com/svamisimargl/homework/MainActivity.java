package com.svamisimargl.homework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_STUDENT = "com.svamisimargl.homework.extra.TEXT";

    Student student = new Student("Виктор","Викторов",21);

    TextView name, lastName, age;
    Button show, edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.nameTextView);
        lastName = findViewById(R.id.lastNameTextView);
        age = findViewById(R.id.ageTextView);
        show = findViewById(R.id.showBtn);
        edit = findViewById(R.id.editBtn);


    }

    public void OnClick(View view) {

        switch (view.getId()) {
            case R.id.showBtn: // при нажатии на эту кнопку мы заполняем наши вьюшки значениями из обьекта - студента
                name.setText(student.getName());
                lastName.setText(student.getLastName());
                age.setText(String.valueOf(student.getAge()));
                break;
            case R.id.editBtn: // при нажатии на эту кнопку мы:
                Intent intent = new Intent(this, Activity2.class);// переходим на активити №2
                intent.putExtra(EXTRA_STUDENT, student); // и передаем значения нашего обьекта используя статическую переменную для имени (ключа) и обьект - как значение
                startActivity(intent); //запускаем в действие
                break;
        }

    }



}

